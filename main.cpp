//////////////////////////////////////////////////////
//	GMU - PROJEKT
//
// Jméno:	Tomáš Hajdík
// Login:	xhajdi01
//
// Jméno:   David Novák
// Login:   xnovak1m
//
// Jméno:	Matúš Gajdár
// Login:	xgajda03
//////////////////////////////////////////////////////

// kostra projektu prevzata z 3. cviceni GMU
// statisticky OpenCL zarizeni vzaty z: https://github.com/sivagnanamn/opencl-device-info (neni dulezite pro projekt)

#pragma comment( lib, "OpenCL" )

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define SELECTED_DEVICE_TYPE CL_DEVICE_TYPE_GPU

//Define this if on Linux ROCm 2.0 to prevent crash on large arrays
//#define LINUX_ROCM_2_0

// only run bitonic sort if not on Linux ROCm 2.0 platform - it crashes on sizes 1024*128 and larger on RX 580 with following error message:
//    Memory access fault by GPU node-1 (Agent handle: 0x55df81ce7660) on address 0x501000000. Reason: Page not present or supervisor privilege.
// works without any problem on Windows on all platforms & works for arrays of size 1024*64 and smaller on Linux

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <CL/cl.hpp>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif //WIN32

#include "oclHelper.h"

void printArray(cl_int *sorting_array, int n){
	for (int i=0; i < n; i++) printf("%d\n", sorting_array[i]);
	printf("\n");
}

void printArrayS(cl_int *sorting_array, cl_int *sorting_array2, int n) {
	int num_of_incorrect = 0;
	for (int i = 0; i < n; i++) {
		if (sorting_array[i] != sorting_array2[i]) {
			num_of_incorrect++;
			printf("INCORRECT %d___%d\n", sorting_array[i], sorting_array2[i]);
		}
		else {
			printf("%d___%d\n", sorting_array[i], sorting_array2[i]);
		}

	}
	printf("Num of incorrect %d\n",num_of_incorrect);
}

void swap(cl_int* a, cl_int* b){
	cl_int t = *a;
	*a = *b;
	*b = t;
}

int partition (cl_int *sorting_array, int min, int max){
	cl_int pivot = sorting_array[max];
	int i = (min - 1);

	for (int j = min; j <= max- 1; j++)	{
		if (sorting_array[j] <= pivot)		{
			i++;
			swap(&sorting_array[i], &sorting_array[j]);
		}
	}
	swap(&sorting_array[i + 1], &sorting_array[max]);
	return (i + 1);
}

void quickSortNext(cl_int *sorting_array, int min, int max){
	if (min < max){

		int partition_index = partition(sorting_array, min, max);
		quickSortNext(sorting_array, min, partition_index - 1);
		quickSortNext(sorting_array, partition_index + 1, max);
	}
}

void quickSort(cl_int *sorting_array, int max){
	quickSortNext(sorting_array, 0, max-1);
}

void insertionSort(cl_int *sorting_array, int n){
	int i, j;
	cl_int  pick_element;
	for (i = 1; i < n; i++)	{
		j = i-1;
		pick_element = sorting_array[i];

		while (sorting_array[j] > pick_element && j >= 0){
			sorting_array[j+1] = sorting_array[j];
			j = j-1;
		}
		sorting_array[j+1] = pick_element;
	}
}

typedef struct
	{
		cl_int vector_size;
		const size_t local_size;
		const char *info;
	}s_test;


void check_cl_error(cl_int err_num, char const * msg) {
	if (err_num != CL_SUCCESS) {
		printf("[Error] OpenCL error code: %d in %s \n", err_num, msg);
		exit(EXIT_FAILURE);
	}
}

int main()
{
	cl_int err_msg, err_msg2;

	// uprava parametrov testov -> {vector_size, local_size, "info"}
	/*s_test tests[] = { {32,16, " Vector size: 32, Local size 16"},
						{64,32, " Vector size: 64, Local size 32"},
						{128,64, " Vector size: 128, Local size 64"},
						{256,128, " Vector size: 256, Local size 128"},
						{512,256, " Vector size: 512, Local size 256"},
						{1024,256, " Vector size: 1024, Local size 256"},
						{1024*2,256, " Vector size: 1024*2, Local size 256" },
						{1024*4,256, " Vector size: 1024*4, Local size 256" },
						{1024*8,256, " Vector size: 1024*8, Local size 256" },
						{1024*16,256, " Vector size: 1024*16, Local size 256" },
						{1024*32,256, " Vector size: 1024*32, Local size 256" },
						{1024*64,256, " Vector size: 1024*64, Local size 256" },
						{1024*128,256, " Vector size: 1024*128, Local size 256" },
						{1024*256,256, " Vector size: 1024*256, Local size	256"}};*/

	// local_size 64 - optimalni pro RX 580
	s_test tests[] = { {32,16, " Vector size: 32, Local size 16"},
					{64,32, " Vector size: 64, Local size 32"},
					{128,64, " Vector size: 128, Local size 64"},
					{256,64, " Vector size: 256, Local size 64"},
					{512,64, " Vector size: 512, Local size 64"},
					{1024,64, " Vector size: 1024, Local size 64"},
					{1024*2,64, " Vector size: 1024*2, Local size 64"},
					{1024*4,64, " Vector size: 1024*4, Local size 64"},
					{1024*8,64, " Vector size: 1024*8, Local size 64"},
					{1024*16,64, " Vector size: 1024*16, Local size 64"},
					{1024*32,64, " Vector size: 1024*32, Local size 64"},
					{1024*64,64, " Vector size: 1024*64, Local size 64"},
					{1024*128,64, " Vector size: 1024*128, Local size 64"},
					{1024*256,64, " Vector size: 1024*256, Local size 64"}};

	/*s_test tests[] = { {32,16, " Vector size: 32, Local size 16"},
					{64,32, " Vector size: 64, Local size 32"},
					{128,32, " Vector size: 128, Local size 32"},
					{256,32, " Vector size: 256, Local size 32"},
					{512,32, " Vector size: 512, Local size 32"},
					{1024,32, " Vector size: 1024, Local size 32"},
					{1024*2,32, " Vector size: 1024*2, Local size 32"},
					{1024*4,32, " Vector size: 1024*4, Local size 32"},
					{1024*8,32, " Vector size: 1024*8, Local size 32"},
					{1024*16,32, " Vector size: 1024*16, Local size 32"},
					{1024*32,32, " Vector size: 1024*32, Local size 32"},
					{1024*64,32, " Vector size: 1024*64, Local size 32"},
					{1024*128,32, " Vector size: 1024*128, Local size 32"},
					{1024*256,32, " Vector size: 1024*256, Local size 32"}};*/

	// spustenie jednotlivych testov
	//s_test tests[] = {{32,16, " Vector size: 32, Local size 16"}};
	//s_test tests[] = {{64,32, " Vector size: 64, Local size 32"}};
	//s_test tests[] = {{512,32, " Vector size: 256, Local size 16"}};
	//s_test tests[] = {{256,128, " Vector size: 256, Local size 128"}};
	//s_test tests[] = {{512,256, " Vector size: 512, Local size 256"}};
	//s_test tests[] = {{1024,32, " Vector size: 1024, Local size 256"}};
	//s_test tests[] = {{2048,256, " Vector size: 2048, Local size 256"}};

	//s_test tests[] = { {1024*256,64, " Vector size: 1024*256, Local size 64"} };
	//s_test tests[] = { {1024*512,64, " Vector size: 1024*512, Local size 64"} };
	//s_test tests[] = { {1024*1024,64, " Vector size: 1024*1024, Local size 64"} };

	// pocet spustenych testov
	size_t TEST_COUNT = sizeof(tests) / sizeof(tests[0]);

	// statisticky OpenCL zarizeni vzaty z: https://github.com/sivagnanamn/opencl-device-info
	printf("\nOpenCL device statistics: \n");

	bool show_stats = false;

	if (!show_stats)
		printf("To show OpenCL device stats, change variable show_stats to true.\n");

	printf("------------------------------\n");

	if (show_stats)
	{
		cl_int err_num;
		char str_buffer[1024];
		cl_uint num_platforms_available;

		// Get the number of OpenCL capable platforms available
		err_num = clGetPlatformIDs(0, NULL, &num_platforms_available);
		check_cl_error(err_num, "clGetPlatformIDs: Getting number of available platforms");

		// Exit if no OpenCL capable platform found
		if (num_platforms_available == 0) {
			printf("No OpenCL capable platforms found ! \n");
			return EXIT_FAILURE;
		}
		else {
			printf("\n Number of OpenCL capable platforms available: %d \n", num_platforms_available);
			printf("--------------------------------------------------\n\n");
		}

		// Create a list for storing the platform id's
		cl_platform_id cl_platforms[10];

		err_num = clGetPlatformIDs(num_platforms_available, cl_platforms, NULL);
		check_cl_error(err_num, "clGetPlatformIDs: Getting available platform id's");

		// Get attributes of each platform available
		for (unsigned platform_idx = 0; platform_idx < num_platforms_available; platform_idx++) {
			printf("\t Platform ID: %d \n", platform_idx);
			printf("\t ----------------\n\n");

			// Get platform name
			err_num = clGetPlatformInfo(cl_platforms[platform_idx], CL_PLATFORM_NAME, sizeof(str_buffer), &str_buffer, NULL);
			check_cl_error(err_num, "clGetPlatformInfo: Getting platform name");
			printf("\t\t [Platform %d] CL_PLATFORM_NAME: %s\n", platform_idx, str_buffer);

			// Get platform vendor
			err_num = clGetPlatformInfo(cl_platforms[platform_idx], CL_PLATFORM_VENDOR, sizeof(str_buffer), &str_buffer, NULL);
			check_cl_error(err_num, "clGetPlatformInfo: Getting platform vendor");
			printf("\t\t [Platform %d] CL_PLATFORM_VENDOR: %s\n", platform_idx, str_buffer);

			// Get platform OpenCL version
			err_num = clGetPlatformInfo(cl_platforms[platform_idx], CL_PLATFORM_VERSION, sizeof(str_buffer), &str_buffer, NULL);
			check_cl_error(err_num, "clGetPlatformInfo: Getting platform version");
			printf("\t\t [Platform %d] CL_PLATFORM_VERSION: %s\n", platform_idx, str_buffer);

			// Get platform OpenCL profile
			err_num = clGetPlatformInfo(cl_platforms[platform_idx], CL_PLATFORM_PROFILE, sizeof(str_buffer), &str_buffer, NULL);
			check_cl_error(err_num, "clGetPlatformInfo: Getting platform profile");
			printf("\t\t [Platform %d] CL_PLATFORM_PROFILE: %s\n", platform_idx, str_buffer);

			// Get platform OpenCL supported extensions
			err_num = clGetPlatformInfo(cl_platforms[platform_idx], CL_PLATFORM_EXTENSIONS, sizeof(str_buffer), &str_buffer, NULL);
			check_cl_error(err_num, "clGetPlatformInfo: Getting platform supported extensions");
			printf("\t\t [Platform %d] CL_PLATFORM_EXTENSIONS: %s\n", platform_idx, str_buffer);

			// Get the number of OpenCL supported device available in this platform
			cl_uint num_devices_available;
			err_num = clGetDeviceIDs(cl_platforms[platform_idx], CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices_available);
			check_cl_error(err_num, "clGetDeviceIDs: Get number of OpenCL supported devices available");
			printf("\n\t\t [Platform %d] Number of devices available: %d \n", platform_idx, num_devices_available);
			printf("\t\t ---------------------------------------------\n\n");

			cl_device_id cl_devices[10];
			err_num = clGetDeviceIDs(cl_platforms[platform_idx], CL_DEVICE_TYPE_ALL, num_devices_available, cl_devices, NULL);
			check_cl_error(err_num, "clGetDeviceIDs: Getting available OpenCL capable device id's");

			// Get attributes of each device
			for (unsigned device_idx = 0; device_idx < num_devices_available; device_idx++) {

				printf("\t\t\t [Platform %d] Device ID: %d\n", platform_idx, device_idx);
				printf("\t\t\t ---------------------------\n\n");

				// Get device name
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_NAME, sizeof(str_buffer), &str_buffer, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device name");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_NAME: %s\n", platform_idx, device_idx, str_buffer);

				// Get device hardware version
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_VERSION, sizeof(str_buffer), &str_buffer, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device hardware version");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_VERSION: %s\n", platform_idx, device_idx, str_buffer);

				// Get device software version
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DRIVER_VERSION, sizeof(str_buffer), &str_buffer, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device software version");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DRIVER_VERSION: %s\n", platform_idx, device_idx, str_buffer);

				// Get device OpenCL C version
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_OPENCL_C_VERSION, sizeof(str_buffer), &str_buffer, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device OpenCL C version");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_OPENCL_C_VERSION: %s\n", platform_idx, device_idx, str_buffer);

				// Get device max clock frequency
				cl_uint max_clock_freq;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(max_clock_freq), &max_clock_freq, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device max clock frequency");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_MAX_CLOCK_FREQUENCY: %d MHz\n", platform_idx, device_idx, max_clock_freq);

				// Get device max compute units available
				cl_uint max_compute_units_available;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(max_compute_units_available), &max_compute_units_available, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device max compute units available");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_MAX_COMPUTE_UNITS: %d\n", platform_idx, device_idx, max_compute_units_available);

				// Get device global mem size
				cl_ulong global_mem_size;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(global_mem_size), &global_mem_size, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device global mem size");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_GLOBAL_MEM_SIZE: %llu MB\n", platform_idx, device_idx, (unsigned long long)global_mem_size / (1024 * 1024));

				// Get device max compute units available
				cl_ulong max_mem_alloc_size;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(max_mem_alloc_size), &max_mem_alloc_size, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device max mem alloc size");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_MAX_MEM_ALLOC_SIZE: %llu MB\n", platform_idx, device_idx, (unsigned long long)max_mem_alloc_size / (1024 * 1024));

				// Get device local mem size
				cl_ulong local_mem_size;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(local_mem_size), &local_mem_size, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device local mem size");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_LOCAL_MEM_SIZE: %llu KB\n", platform_idx, device_idx, (unsigned long long)local_mem_size / 1024);

				// Get device max work group size
				size_t max_work_group_size;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_work_group_size), &max_work_group_size, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device max work group size");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_MAX_WORK_GROUP_SIZE: %ld\n", platform_idx, device_idx, (long int)max_work_group_size);

				// Get device max work item dim
				cl_uint max_work_item_dims;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(max_work_item_dims), &max_work_item_dims, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device max work item dimension");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS: %d\n", platform_idx, device_idx, max_work_item_dims);

				// Get device max work item sizes in each dimension
				size_t work_item_sizes[10000];
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(work_item_sizes), &work_item_sizes, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device max work items dimension");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_MAX_WORK_ITEM_SIZES: ", platform_idx, device_idx);
				for (size_t work_item_dim = 0; work_item_dim < max_work_item_dims; work_item_dim++) {
					printf("%ld ", (long int)work_item_sizes[work_item_dim]);
				}
				printf("\n");

				// Get device image support
				cl_bool image_support;
				err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_IMAGE_SUPPORT, sizeof(image_support), &image_support, NULL);
				check_cl_error(err_num, "clGetDeviceInfo: Getting device image support");
				printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_IMAGE_SUPPORT: %u (%s)\n", platform_idx, device_idx, image_support, image_support ? "Available" : "Not available");

				if (image_support) {

					size_t image_size;

					// Get device image 2d max width
					err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(image_size), &image_size, NULL);
					check_cl_error(err_num, "clGetDeviceInfo: Getting device image max 2d width");
					printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_IMAGE2D_MAX_WIDTH: %ld\n", platform_idx, device_idx, (long int)image_size);

					// Get device image 2d max height
					err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(image_size), &image_size, NULL);
					check_cl_error(err_num, "clGetDeviceInfo: Getting device image max 2d width");
					printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_IMAGE2D_MAX_HEIGHT: %ld\n", platform_idx, device_idx, (long int)image_size);

					// Get device image 3d max width
					err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(image_size), &image_size, NULL);
					check_cl_error(err_num, "clGetDeviceInfo: Getting device image max 3d width");
					printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_IMAGE3D_MAX_WIDTH: %ld\n", platform_idx, device_idx, (long int)image_size);

					// Get device image 3d max height
					err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(image_size), &image_size, NULL);
					check_cl_error(err_num, "clGetDeviceInfo: Getting device image max 3d height");
					printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_IMAGE3D_MAX_HEIGHT: %ld\n", platform_idx, device_idx, (long int)image_size);

					// Get device image 2d max depth
					err_num = clGetDeviceInfo(cl_devices[device_idx], CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(image_size), &image_size, NULL);
					check_cl_error(err_num, "clGetDeviceInfo: Getting device image max 3d depth");
					printf("\t\t\t\t\t [Platform %d] [Device %d] CL_DEVICE_IMAGE3D_MAX_DEPTH: %ld\n", platform_idx, device_idx, (long int)image_size);

				}
				printf("\n\n");
			}
		}
	}

	//============================= konec kodu pro zobrazeni OpenCL statistik ====================================

	std::vector<cl::Platform> platforms;
	std::vector<cl::Device> platform_devices;
	// Get Platforms count
	clPrintErrorExit(cl::Platform::get(&platforms), "cl::Platform::get");
	printf("Platforms:\n");
	for (unsigned int i = 0; i < platforms.size(); i++)
	{
		// Print platform name
		printf(" %d. platform name: %s.\n", i, platforms[i].getInfo<CL_PLATFORM_NAME>(&err_msg).c_str());
		clPrintErrorExit(err_msg, "cl::Platform::getInfo<CL_PLATFORM_NAME>");

		// Get platform devices count
		clPrintErrorExit(platforms[i].getDevices(CL_DEVICE_TYPE_ALL, &platform_devices), "getDevices");
		for (unsigned int j = 0; j < platform_devices.size(); j++)
		{
			// Get device name
			printf("  %d. device name: %s.\n", j, platform_devices[j].getInfo<CL_DEVICE_NAME>(&err_msg).c_str());
			clPrintErrorExit(err_msg, "cl::Device::getInfo<CL_DEVICE_NAME>");
		}
		platform_devices.clear();
	}

	cl::Device selected_device;
	bool device_found = false;
	for (unsigned int i = 0; i < platforms.size(); i++)
	{
		clPrintErrorExit(platforms[i].getDevices(SELECTED_DEVICE_TYPE, &platform_devices), "getDevices");
		if (platform_devices.size() != 0)
		{
			device_found = true;
			selected_device = platform_devices[0];
			break;
		}
	}
	if (!device_found) clPrintErrorExit(CL_DEVICE_NOT_FOUND, "GPU device");

	// check if device is correct
	if (selected_device.getInfo<CL_DEVICE_TYPE>() == SELECTED_DEVICE_TYPE)
	{
		printf("\nSelected device type: Correct\n");
	}
	else
	{
		printf("\nSelected device type: Incorrect\n");
	}
	printf("Selected device name: %s.\n", selected_device.getInfo<CL_DEVICE_NAME>().c_str());

	platforms.clear();

	cl::Context context(selected_device, NULL, NULL, NULL, &err_msg);
	clPrintErrorExit(err_msg, "cl::Context");

	cl::CommandQueue queue(context, selected_device, CL_QUEUE_PROFILING_ENABLE, &err_msg);
	clPrintErrorExit(err_msg, "cl::CommandQueue");

	char *program_source = readFile("../array_sort.cl");
	cl::Program::Sources sources;
	sources.push_back(std::pair<const char *, size_t>(program_source, 0));

	cl::Program program(context, sources, &err_msg);
	clPrintErrorExit(err_msg, "clCreateProgramWithSource");

	// build program
	if ((err_msg = program.build(std::vector<cl::Device>(1, selected_device), "", NULL, NULL)) == CL_BUILD_PROGRAM_FAILURE)
	{
		printf("Build log:\n %s", program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(selected_device, &err_msg2).c_str());
		clPrintErrorExit(err_msg2, "cl::Program::getBuildInfo<CL_PROGRAM_BUILD_LOG>");
	}
	clPrintErrorExit(err_msg, "clBuildProgram");

	cl::make_kernel<cl::Buffer&, cl::Buffer&, cl_int&> selection_sort = cl::make_kernel<cl::Buffer&, cl::Buffer&, cl_int&>(program, "selection_sort", &err_msg);
	cl::make_kernel<cl::Buffer&, cl::Buffer&, cl::LocalSpaceArg, cl_int&> selection_sort_local = cl::make_kernel<cl::Buffer&, cl::Buffer&, cl::LocalSpaceArg, cl_int&>(program, "selection_sort_local", &err_msg);
	cl::make_kernel<cl::Buffer&, cl_int&> odd_sort = cl::make_kernel<cl::Buffer&, cl_int&>(program, "odd_sort", &err_msg);
	cl::make_kernel<cl::Buffer&, cl_int&> even_sort = cl::make_kernel<cl::Buffer&, cl_int&>(program, "even_sort", &err_msg);
	cl::make_kernel<cl::Buffer&, cl_int&, cl_int&> bitonic_sort = cl::make_kernel<cl::Buffer&, cl_int&, cl_int&>(program, "bitonic_sort", &err_msg);
	cl::make_kernel<cl::Buffer&, cl::LocalSpaceArg> merge_sort = cl::make_kernel<cl::Buffer&, cl::LocalSpaceArg>(program, "merge_sort", &err_msg);
	cl::make_kernel<cl::Buffer&, cl::Buffer&, cl_int&, cl_int&> merge_sort_seq = cl::make_kernel<cl::Buffer&, cl::Buffer&, cl_int&, cl_int&>(program, "merge_sort_seq", &err_msg);
	cl::make_kernel<cl::Buffer&, cl::Buffer&, cl_int&> merge_sort_global = cl::make_kernel<cl::Buffer&, cl::Buffer&, cl_int&>(program, "merge_sort_global", &err_msg);
	cl::make_kernel<cl::Buffer&, cl::LocalSpaceArg> bitonic_sort_local = cl::make_kernel<cl::Buffer&, cl::LocalSpaceArg>(program, "bitonic_sort_local", &err_msg);

	// cl::Kernel kernel(program, "selection_sort", &err_msg);
	clPrintErrorExit(err_msg, "cl::Kernel");

	for (unsigned i = 0; i < TEST_COUNT; i++){

		cl_int vector_size = tests[i].vector_size;
		const size_t local_size =  tests[i].local_size;

		// Create host buffers
		cl_int *input_data = genRandomBuffer(vector_size);

		// sel_sort_out_device_data - vysledok selection sort spočítaný na GPU
		cl_int *sel_sort_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		// sel_sort_local_out_device_data - vysledok selection sort s local mem spočítaný na GPU
		cl_int *sel_sort_local_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		// odd_even_sort_out_device_data - vysledok odd even sort spočítaný na GPU
		cl_int *odd_even_sort_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		// bitonic_sort_local_out_device_data - vysledok bitonic sort local spočítaný na GPU
		cl_int *bitonic_sort_local_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		// merge_sort_out_device_data - výsledek merge sort spočítaný na GPU
		cl_int *merge_sort_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		// merge_sort_global_out_device_data - výsledek merge sort spočítaný na GPU
		cl_int *merge_sort_global_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		// merge_sort_global_out_device_data - výsledek merge sort spočítaný na GPU
		cl_int *merge_sort_global_local_out_device_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);

		cl_int *insert_sort_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);
		memcpy(insert_sort_data, input_data, sizeof(cl_int) * vector_size);

		cl_int *quick_sort_data = (cl_int *)malloc(sizeof(cl_int) * vector_size);
		memcpy(quick_sort_data, input_data, sizeof(cl_int) * vector_size);


		// =======================================================
		// vytvorit buffery
		// =======================================================
		cl::Buffer in_buff;
		cl::Buffer out_buff;
		cl::Buffer out_buff_loc;
		cl::Buffer out_buff_odd_even;
		cl::Buffer out_buff_bitonic_local;
		cl::Buffer out_buff_merge;
		cl::Buffer out_buff_merge_global;

		in_buff = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);
		out_buff = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);
		out_buff_loc = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);
		out_buff_odd_even = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);
		out_buff_bitonic_local = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);
		out_buff_merge = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);
		out_buff_merge_global = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_int) * vector_size);

		// =======================================================
		// nastavit eventy
		// =======================================================
		// Eventy pro kopirovani na zarizeni
		cl::UserEvent x_write_buffer_event(context, &err_msg);
		clPrintErrorExit(err_msg, "clCreateUserEvent x_write_buffer_event");
		// Event pro kopirovani ze zarizeni
		cl::UserEvent y_read_buffer_event(context, &err_msg);
		clPrintErrorExit(err_msg, "clCreateUserEvent y_read_buffer_event");



		// ======================================================
		//			SORTING PART GPU
		// ======================================================
		// ======================================================
		// nastavenie velkosti skupiny
		// zarovnání s funkciou alignTo(co, na_nasobek_ceho)
		// kopirovanie dat na GPU - x_write_buffer_event
		// spustenie kernelov
		// kopírovanie dát zGPU - y_read_buffer_event
		// synchronizácia queue
		// =======================================================
		cl::NDRange local(local_size);
		cl::NDRange global(alignTo(vector_size, local_size));


		clPrintErrorExit(queue.enqueueWriteBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), input_data, NULL, &x_write_buffer_event),"clEnqueueWriteBuffer: in_buff");

		cl::Event kernel_sel_sort_event = selection_sort(cl::EnqueueArgs(queue, global), out_buff, in_buff, vector_size);
		clPrintErrorExit(queue.enqueueReadBuffer(out_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), sel_sort_out_device_data, NULL, &y_read_buffer_event),"enqueueReadBuffer: out_buff");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");


		cl::Event kernel_sel_sort_local_event = selection_sort_local(cl::EnqueueArgs(queue, global, local), out_buff_loc, in_buff, cl::Local(local_size*4), vector_size);
		clPrintErrorExit(queue.enqueueReadBuffer(out_buff_loc, CL_FALSE, 0, vector_size * sizeof(cl_int), sel_sort_local_out_device_data, NULL, &y_read_buffer_event),"enqueueReadBuffer: out_buff_loc");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");


		// ======================================================
		//			SORTING ODD-EVEN
		// ======================================================
		cl_int iter;
		double time_odd_even = 0.0;
		for (iter = 0; iter < vector_size/2; iter++)
		{
			cl::Event kernel_even_sort_event = even_sort(cl::EnqueueArgs(queue, global), in_buff, vector_size);
			cl::Event kernel_odd_sort_event = odd_sort(cl::EnqueueArgs(queue, global), in_buff, vector_size);

			clPrintErrorExit(queue.enqueueReadBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), odd_even_sort_out_device_data, NULL, &y_read_buffer_event),"enqueueReadBuffer: out_buff_odd_even");
			// synchronize queue
			clPrintErrorExit(queue.finish(), "clFinish");
			time_odd_even += getEventTime(kernel_even_sort_event)*1000;
			time_odd_even += getEventTime(kernel_odd_sort_event)*1000;
		}
		//!!!reinitialize input buffer
		clPrintErrorExit(queue.enqueueWriteBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), input_data, NULL, &x_write_buffer_event),"clEnqueueWriteBuffer: in_buff");


		// ======================================================
		//			MERGE SORT
		//
		// Jméno:	David Novák
		// Login:	xnovak1m
		// ======================================================
		double time_merge = 0.0;

		// roztridit cele pole do sekvenci o velikosti local_size
		cl::Event kernel_merge_sort_event = merge_sort(cl::EnqueueArgs(queue, global, local), in_buff, cl::Local(local_size * 4));

		clPrintErrorExit(queue.enqueueReadBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), merge_sort_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: out_buff_merge");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");

		time_merge += getEventTime(kernel_merge_sort_event) * 1000;

		// merge roztrizenych sekvenci (sekvencne)
		for (int len = local_size * 2, threads = vector_size / local_size / 2; len <= vector_size; len *= 2, threads /= 2)
		{
			//printf("len = %d, threads = %d\n", len, threads);
			kernel_merge_sort_event = merge_sort_seq(cl::EnqueueArgs(queue, global), out_buff_merge, in_buff, len, threads);

			// zde by se asi dalo misto kopirovani buffery jen prohazovat
			clPrintErrorExit(queue.enqueueCopyBuffer(out_buff_merge, in_buff, 0, 0, vector_size * sizeof(cl_int), 0, NULL), "enqueueCopyBuffer: out_buff_merge -> in_buff");
			// synchronize queue
			clPrintErrorExit(queue.finish(), "clFinish");

			time_merge += getEventTime(kernel_merge_sort_event) * 1000;
		}

		clPrintErrorExit(queue.enqueueReadBuffer(out_buff_merge, CL_FALSE, 0, vector_size * sizeof(cl_int), merge_sort_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: out_buff_merge");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");


		//!!!reinitialize input buffer
		clPrintErrorExit(queue.enqueueWriteBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), input_data, NULL, &x_write_buffer_event), "clEnqueueWriteBuffer: in_buff");

		// =====================
		// GLOBAL version
		// =====================
		double time_merge_global = 0.0;

		for (int len = 1; len < vector_size; len *= 2)
		{
			cl::Event kernel_merge_sort_global_event = merge_sort_global(cl::EnqueueArgs(queue, global), out_buff_merge_global, in_buff, len);

			clPrintErrorExit(queue.enqueueCopyBuffer(out_buff_merge_global, in_buff, 0, 0, vector_size * sizeof(cl_int), 0, NULL), "enqueueCopyBuffer: out_buff_merge_global -> in_buff");

			// synchronize queue
			clPrintErrorExit(queue.finish(), "clFinish");

			time_merge_global += getEventTime(kernel_merge_sort_global_event) * 1000;
		}

		clPrintErrorExit(queue.enqueueReadBuffer(out_buff_merge_global, CL_FALSE, 0, vector_size * sizeof(cl_int), merge_sort_global_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: out_buff_merge");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");

		//!!!reinitialize input buffer
		clPrintErrorExit(queue.enqueueWriteBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), input_data, NULL, &x_write_buffer_event), "clEnqueueWriteBuffer: in_buff");

		// =====================
		// LOCAL + GLOBAL version
		// =====================
		double time_merge_global_local = 0.0;

		// roztridit cele pole do sekvenci o velikosti local_size
		cl::Event kernel_merge_sort_global_local_event = merge_sort(cl::EnqueueArgs(queue, global, local), in_buff, cl::Local(local_size * 4));

		clPrintErrorExit(queue.enqueueReadBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), merge_sort_global_local_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: in_buff");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");

		time_merge_global_local += getEventTime(kernel_merge_sort_global_local_event) * 1000;

		// merge roztrizenych sekvenci (paralelne, v globalni pameti)
		for (int len = local_size; len < vector_size; len *= 2)
		{
			 kernel_merge_sort_global_local_event = merge_sort_global(cl::EnqueueArgs(queue, global), out_buff_merge_global, in_buff, len);

			clPrintErrorExit(queue.enqueueCopyBuffer(out_buff_merge_global, in_buff, 0, 0, vector_size * sizeof(cl_int), 0, NULL), "enqueueCopyBuffer: out_buff_merge_global -> in_buff");

			// synchronize queue
			clPrintErrorExit(queue.finish(), "clFinish");

			time_merge_global += getEventTime(kernel_merge_sort_global_local_event) * 1000;
		}

		clPrintErrorExit(queue.enqueueReadBuffer(out_buff_merge_global, CL_FALSE, 0, vector_size * sizeof(cl_int), merge_sort_global_local_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: in_buff");
		// synchronize queue
		clPrintErrorExit(queue.finish(), "clFinish");

		//!!!reinitialize input buffer
		clPrintErrorExit(queue.enqueueWriteBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), input_data, NULL, &x_write_buffer_event), "clEnqueueWriteBuffer: in_buff");



		// ======================================================
		//			SORTING BITONIC GLOBAL + LOCAL
		//
		// Jméno:	Matúš Gajdár
		// Login:	xgajda03
		// ======================================================
		int num_of_phases = 0;
		int cnt = vector_size;
		//get number of phases based on size of vector, number of k in 2^k == vector_size
		do {
			if (cnt % 2) {
				printf("[WARN] Bitonic sort: Array size is not size=2^k, for k = 1..\n");
				num_of_phases = 0;
				break;
			}
			cnt /= 2;   num_of_phases++;
		} while (cnt != 1);

		cl_int phase, subPhase;
		double time_bitonic = 0.0;
	// only run bitonic sort if not on Linux ROCm 2.0 platform - it crashes on sizes 1024*128 and larger on RX 580 with following error message:
	//    Memory access fault by GPU node-1 (Agent handle: 0x55df81ce7660) on address 0x501000000. Reason: Page not present or supervisor privilege.
	// works without any problem on Windows on all platforms & works for arrays of size 1024*64 and smaller on Linux
	#ifndef LINUX_ROCM_2_0
		for (phase = 0; phase < num_of_phases; ++phase) {
			cl::Event kernel_bitonic_sort_local_event;
			unsigned blockWidth = (1 << (phase)) << 1;
			//first blocks to local size sort by local bitonic
			if (blockWidth == local_size) {
				kernel_bitonic_sort_local_event = bitonic_sort_local(cl::EnqueueArgs(queue, global, local), in_buff, cl::Local(blockWidth * 4));
				clPrintErrorExit(queue.enqueueReadBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), bitonic_sort_local_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: out_buff_bitonic");
				// synchronize queue
				clPrintErrorExit(queue.finish(), "clFinish");
				time_bitonic += (getEventTime(kernel_bitonic_sort_local_event)) * 1000;
			}
			//other blocks sort by global bitonic
			else {
				for (subPhase = 0; subPhase < phase + 1; subPhase++) {
					kernel_bitonic_sort_local_event = bitonic_sort(cl::EnqueueArgs(queue, global), in_buff, phase, subPhase);
					clPrintErrorExit(queue.enqueueReadBuffer(in_buff, CL_FALSE, 0, vector_size * sizeof(cl_int), bitonic_sort_local_out_device_data, NULL, &y_read_buffer_event), "enqueueReadBuffer: out_buff_bitonic");
					// synchronize queue
					clPrintErrorExit(queue.finish(), "clFinish");
					time_bitonic += (getEventTime(kernel_bitonic_sort_local_event)) * 1000;
				}
			}
		}
	#endif

		// ======================================================
		//			END OF SORTING BITONIC
		// ======================================================



		// ======================================================
		//			SORTING PART CPU
		// ======================================================
		double quick_cpu_start = getTime();
		quickSort(quick_sort_data, vector_size);
		double quick_cpu_end = getTime();

		double insertion_cpu_start = getTime();
		insertionSort(insert_sort_data, vector_size);
		double insertion_cpu_end = getTime();
		// ======================================================
		//			END SORTING PART
		// ======================================================



		// ======================================================
		// print results
		// ======================================================
		printf("\nExample results:\n");
		// for (int i = 0; i < vector_size; i++)
		// {
		// 	printf(" [%d] input_data %d\t  %d(gpu) \t %d(cpu)\n", i, input_data[i], sel_sort_local_out_device_data[i], quick_sort_data[i]);
		// }
		printf("%s, Global size %d\n", tests[i].info, alignTo(vector_size,256));

		// ======================================================
		// print performance info
		// ======================================================
		printf("Host timers:\n");
		printf(" CPU    quickSort time: %f ms\n",(quick_cpu_end - quick_cpu_start)*1000);
		printf(" CPU    insertionSort time: %f ms\n", (insertion_cpu_end - insertion_cpu_start)*1000);

		printf("Device timers:\n");
		printf(" OpenCL copy time: %f ms\n", (getEventTime(x_write_buffer_event) + getEventTime(y_read_buffer_event))*1000);
		printf(" OpenCL selection sort time:  %f ms\n", (getEventTime(kernel_sel_sort_event))*1000);
		printf(" OpenCL sel sort local time:  %f ms\n", (getEventTime(kernel_sel_sort_local_event))*1000);
		printf(" OpenCL odd-even sort  time:  %f ms\n", time_odd_even);
		printf(" OpenCL bitonic sort (local + global) time:  %f ms\n", time_bitonic);
		printf(" OpenCL merge sort (local + seq) time:  %f ms\n", time_merge);
		printf(" OpenCL merge sort (global) time:  %f ms\n", time_merge_global);
		printf(" OpenCL merge sort (local + global) time:  %f ms\n", time_merge_global_local);

		printf("Compare results:\n");
		// check data quick sort data vs. insert sort data
		if (memcmp(quick_sort_data, insert_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result CPU sorting: Correct\n");
		else
			printf(" Result CPU sorting: Incorrect\n");

		// check selection sort data  vs.  quick sort data
		if (memcmp(sel_sort_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU selection sorting: Correct\n");
		else
			printf(" Result GPU selection sorting: Incorrect\n");

		// check selection local sort data  vs.  quick sort data
		if (memcmp(sel_sort_local_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU selection_local sorting: Correct\n");
		else
			printf(" Result GPU selection_local sorting: Incorrect\n");

		// check odd-even sort data  vs.  quick sort data
		if (memcmp(odd_even_sort_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU odd-even sorting: Correct\n");
		else {
			printf(" Result GPU odd-even sorting: Incorrect\n");
			//printArrayS(odd_even_sort_out_device_data, quick_sort_data, vector_size);
		}

		if (memcmp(bitonic_sort_local_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU bitonic (local + global) sorting: Correct\n");
		else {
			printf(" Result GPU bitonic (local + global) sorting: Incorrect\n");
			//printArrayS(bitonic_sort_local_out_device_data, quick_sort_data, vector_size);
		}

		// check merge sort data vs. quick sort data
		if (memcmp(merge_sort_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU merge sorting (local + seq): Correct\n");
		else {
			printf(" Result GPU merge sorting (local + seq): Incorrect\n");
			//printArrayS(merge_sort_out_device_data, quick_sort_data, vector_size);
		}

		// check merge sort (global) data vs. quick sort data
		if (memcmp(merge_sort_global_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU merge sorting (global): Correct\n");
		else {
			printf(" Result GPU merge sorting (global): Incorrect\n");
			//printArrayS(merge_sort_global_out_device_data, quick_sort_data, vector_size);
		}

		// check merge sort (local + global) data vs. quick sort data
		if (memcmp(merge_sort_global_local_out_device_data, quick_sort_data, vector_size * sizeof(cl_int)) == 0)
			printf(" Result GPU merge sorting (local + global): Correct\n");
		else {
			printf(" Result GPU merge sorting (local + global): Incorrect\n");
			//printArrayS(merge_sort_global_local_out_device_data, quick_sort_data, vector_size);
		}

		// deallocate host data
		free(input_data);
		free(insert_sort_data);
		free(quick_sort_data);
		free(sel_sort_out_device_data);
		free(sel_sort_local_out_device_data);
		free(odd_even_sort_out_device_data);
		free(bitonic_sort_local_out_device_data);
		free(merge_sort_out_device_data);
		free(merge_sort_global_out_device_data);

		printf("\n=========================================\n");
	}

	printf("all tests completed\n");

	return 0;
}

