//////////////////////////////////////////////////////
//	GMU - PROJEKT
//
// Meno:	Tomáš Hajdík
// Login:	xhajdi01 
//////////////////////////////////////////////////////

__kernel void selection_sort(__global int *output, const __global int *input, int vector_size){
	int id = (int)get_global_id(0);

	if (id >= vector_size) return;

	int position = 0;
	for (int j=0 ; j<vector_size ; j++)	{
		position += (( input[id] > input[j]) || (input[j] == input[id] && (j < id))) ? 1 : 0;
	}		
	output[position] = input[id];

}


__kernel void selection_sort_local(__global int *output, const __global int *input, __local int *local_mem, int vector_size){
	int global_id = (int)get_global_id(0);
	int local_id = (int)get_local_id(0);
	int local_size = (int)get_local_size(0);
	// printf("GlobalID %d, local size %d, LocalID %d\n", global_id,local_size,local_id);


	int in_global_id;
	if (global_id < vector_size) 
		in_global_id = input[global_id];
	int position = 0;
	for (int j = 0; j < vector_size; j += local_size){
		barrier(CLK_LOCAL_MEM_FENCE);
		local_mem[local_id] = input[j+local_id];
		barrier(CLK_LOCAL_MEM_FENCE);
		if (global_id < vector_size)
			for (int index = 0 ; index < local_size ; index++){
				int local_mem_index = local_mem[index];
				position += (( in_global_id > local_mem_index) || (local_mem_index == in_global_id && ((j+index) < global_id))) ? 1 : 0;
			}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if (global_id < vector_size)
		output[position] = in_global_id;
}


__kernel void odd_sort(__global int *output, int vector_size){
	int idx = (int)get_global_id(0);
	idx = idx *2 + 1;

	if (idx < (vector_size-1)){	// neparny - odd
		if (output[idx] > output[idx+1]){
			// swap values
			int tmp=output[idx];
			output[idx]=output[idx+1];
			output[idx+1]=tmp;
		}
	}
}__kernel void even_sort(__global int *output, int vector_size){
	int idx = (int)get_global_id(0);
	idx = idx *2;

	if (idx < (vector_size-1)){	// parny - even
		if (output[idx] > output[idx+1]){
			// swap values
			int tmp=output[idx];
			output[idx]=output[idx+1];
			output[idx+1]=tmp;
		}
	}
}

// ======================================================
//			SORTING BITONIC
//
// Jméno:	Matúš Gajdár
// Login:	xgajda03
// ======================================================

__kernel void bitonic_sort(__global int *input, int phase, int subPhase) {

	uint global_id = get_global_id(0);
	uint sort_switch = true;

	//------ splitting sequence to bitonic sequences
	uint lenBetweenCompEl = 1 << (phase - subPhase);
	uint blockWidth = lenBetweenCompEl << 1;
	
	uint left_id = (global_id % lenBetweenCompEl) + (global_id / lenBetweenCompEl) * blockWidth;
	uint right_id = left_id + lenBetweenCompEl;
	//------

	//------ bitonic sequences is where one half is monotonically increasing and the other half
	//is monotonically decreasing
	uint sameDirectionBlockWidth = 1 << phase;

	if ((global_id / sameDirectionBlockWidth) % 2 == 1)
		sort_switch = (!sort_switch);
	//------

	//------ compare two elements in sequence and assign them
	int leftElement = input[left_id];
	int rightElement = input[right_id];

	int max = (leftElement > rightElement) ? leftElement : rightElement;
	int min = (leftElement > rightElement) ? rightElement : leftElement;

	input[left_id] = sort_switch ? min : max;
	input[right_id] = sort_switch ? max : min;
}

__kernel void bitonic_sort_local(__global int* input, __local int *local_data) {

	int global_id = get_global_id(0);
	int local_id = get_local_id(0);
	int group_size = get_local_size(0);

	int offset = get_group_id(0) * group_size;
	input += offset;

	// Store data to local_data
	local_data[local_id] = input[local_id];
	barrier(CLK_LOCAL_MEM_FENCE);

	//Loop phase (from global)
	for (int length = 1; length < group_size; length <<= 1)
	{
		bool direction = true; //((local_id & (length << 1)) != 0); // direction
		if ((global_id & (length << 1)) != 0) direction = (!direction);
		//Loop subphase (from global)
		for (int inc = length; inc > 0; inc >>= 1)
		{
			int lenBetweenCompEl = local_id ^ inc;
			int left_id = local_data[local_id];
			int right_id = local_data[lenBetweenCompEl];
			bool smaller = (left_id < right_id) || (right_id == left_id && lenBetweenCompEl < local_id);
			bool sort_switch = smaller ^ (lenBetweenCompEl < local_id) ^ (direction);
			local_data[local_id] = (sort_switch) ? right_id : left_id;
			barrier(CLK_LOCAL_MEM_FENCE);
		}
	}

	// Write output
	input[local_id] = local_data[local_id];

}

//////////////////////////////////////////////////////
//			MERGE SORT
//
// Jméno:	David Novák
// Login:	xnovak1m 
//////////////////////////////////////////////////////

// paralelni merge az po velikost local_size
// inspirace (zejmena vyhledavani a urcovani cilove adresy): http://www.bealto.com/gpu-sorting_parallel-merge-local.html
__kernel void merge_sort(__global int *data, __local int *local_data)
{
	int local_id = get_local_id(0);
	int group_id = get_group_id(0);
	int group_size = get_local_size(0);
	int offset = group_id * group_size;

	local_data[local_id] = data[local_id+offset];

	barrier(CLK_LOCAL_MEM_FENCE);

	// paralelni merge od jednotkovych poli - zjisti se nas index a index, na ktery bychom patrili ve druhe skupine
	// dohromady jde o nas index ve vyslednem poli
	for (int len = 1; len < group_size; len *= 2)
	{
		int index = local_id & (len - 1);
		int sibling = (local_id - index) ^ len; // druha sekvence - zde se bude hledat
		int pos = 0;

		// dichotomicke hledani pozice (muzeme, protoze je to serazena sekvence)
		for (int inc = len; inc > 0; inc /= 2)
		{
			int j = sibling + pos + inc - 1;

			if ((local_data[j] < local_data[local_id]) || (local_data[j] == local_data[local_id] && j < local_id))
				pos += inc;
			
			pos = min(pos, len);
		}

		int bits = 2*len - 1;
		int dest = ((index + pos) & bits) | (local_id & ~bits);

		barrier(CLK_LOCAL_MEM_FENCE);
		
		local_data[dest] = local_data[local_id]; // presunout na cilovou pozici
		
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	data[local_id+offset] = local_data[local_id];
}

// paralelni merge v globalni pameti - jen jeden krok
__kernel void merge_sort_global(__global int *out, const __global int *data, int len)
{
	int global_id = get_global_id(0);
	int group_id = get_group_id(0);

	int index = global_id & (len - 1);
	int sibling = (global_id - index) ^ len; // druha sekvence - zde se bude hledat
	int pos = 0;

	// dichotomicke hledani pozice (muzeme, protoze je to serazena sekvence)
	for (int inc = len; inc > 0; inc /= 2)
	{
		int j = sibling + pos + inc - 1;

		if ((data[j] < data[global_id]) || (data[j] == data[global_id] && j < global_id))
			pos += inc;
			
		pos = min(pos, len);
	}

	int bits = 2*len - 1;
	int dest = ((index + pos) & bits) | (global_id & ~bits);
		
	out[dest] = data[global_id]; // nutne pouzit druhy buffer - nemuzeme synchronizovat mezi vlakny v ruznych pracovnich skupinach
}

// sekvenci merge seradenych sekvenci
__kernel void merge_sort_seq(__global int *out, const __global int *in, int len, int threads)
{
	int global_id = get_global_id(0); // index in workgroup
	int offset = global_id * len;

	// zde by se mozna hodilo zajistit, aby se spoustela vlakna z jinych workgroup..?
	if (global_id < threads)
	{
	//printf("global_id: %d, offset: %d, k: %d\n", global_id, offset, offset+(len/2));
		for (int i = offset, j = offset, k = offset+(len/2); i < offset+len; i++)
		{
			// treba hlidat meze j a k
			if ((in[j] <= in[k] && j < offset+(len/2)) || k >= offset+len)
			{
				//printf("i = %d, val = %d\n", i, in[j]);
				out[i] = in[j];
				j++;
			}
			else
			{
				//printf("i = %d, val = %d\n", i, in[k]);
				out[i] = in[k];
				k++;
			}
		}
	}
}