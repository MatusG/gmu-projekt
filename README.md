# GMU projekt
Projekt v ktorom implementujeme rôzne radiace algoritmy 
a porovnávame ich efektivitu. Porovnanie prebieha vzájomne medzi jednotlivými algoritmami a taktiež medzi jednotlivými grafickými kartami na ktorých boli implementované v rámci projektu.

Celý spustiteľný archív s projektom je dostupný online na stránke https://gitlab.com/MatusG/gmu-projekt

### Knižnice
Pri inštalácii potrebných knižníc sme postupovali podľa https://www.fit.vutbr.cz/study/courses/GMU/private/lab/tutorial.html

Předpokládá se existence systémových proměnných prostředí OCL_ROOT nebo CUDA_PATH.

### Súbory
Kostra projektu bola vytvorená z cvičenia GMU: https://www.fit.vutbr.cz/study/courses/GMU/private/lab/cv3/gmu3_cpp.zip

Zmenené súbory:

- main.cpp - obsahuje volania radiacich kernelových programov, ich meranie a kontrolu,
- array_sort.cl - obsahuje kernelové programy, teda jednotlivé radiace algoritmy

Přidané soubory:

- CMakeLists.txt - definice projektu pro překlad pomocí CMake
- FindOpenCL.cmake - kód pro hledání knihovny OpenCL
- dokumentace.pdf - dokumentace projektu

### Spustenie
Pomocí CMake vygeneruje nový projekt pro VS (či jiné překladové prostředí). V případě potřeby je možné použít projekt pro VS, který se nachází ve složce obsolete.
