Projekt do předmětu GMU (Grafické a Multimediální procesory)
VUT FIT, 6. 1. 2019

Git repozitář: https://gitlab.com/MatusG/gmu-projekt

Autoři
------
Matúš Gajdár (xgajda03@stud.fit.vutbr.cz)
Tomáš Hajdík (xhajdi01@stud.fit.vutbr.cz)
David Novák (xnovak1m@stud.fit.vutbr.cz)

Zadání
------
- Nastudujte bitonic, merge sort a další paralelní řadící algoritmy.
- Implementujte 3 řadící algoritmy.
- Porovnejte vykonnostně a zdokumentujte.

Projekt implementuje a měří výkonnost těchto algoritmů:
- Selection sort
- Odd-even sort
- Bitonic sort
- Merge sort

Soubory
-------
main.cpp - obsahuje volania radiacich kernelových programov, ich meranie a kontrolu
array_sort.cl - obsahuje kernelové programy, teda jednotlivé radiace algoritmy
oclHelper.cpp - pomocné funkce
oclHelper.h - hlavičkový soubor pro oclHelper
CMakeLists.txt - definice projektu pro překlad pomocí CMake
dokumentace.pdf - dokumentace projektu
README.txt - tento soubor

Kostra programu byla převzata z 3. cvičení předmětu GMU: https://www.fit.vutbr.cz/study/courses/GMU/private/lab/cv3/gmu3_cpp.zip
Základ CMake projektu byl převzat z: https://gist.github.com/jirihnidek/3f23b7451c1744d71dbf

Návod ke spuštění
-----------------
Upozornění: Předpokládá se existence systémových proměnných prostředí AMDAPPSDKROOT, OCL_ROOT nebo CUDA_PATH. Pokud tyto nejsou definované a OpenCL se nenachází v žádném z výchozích umístění, bude nutné při konfiguraci CMake ručně nastavit cesty.

Vyžadované knihovny:
- OpenCL 1.2 (dle výrobce GPU)
- OpenMP (https://www.openmp.org/); součást MS VS; na Linuxu knihovna lgomp

1. Spusťte CMake (testováno na Ubuntu - 3.10 a Windows - 3.13)
2. Nastavte cestu ke zdrojovým souborům na adresář s tímto souborem
3. Nastavte cestu pro překlad na podadresář (např. build)
4. Konfigurace CMake, zvolit 64bit překladové prostředí (testováno na MS VS 2017 64bit a GCC 7)
4a. Přepnout na Release (ale Debug je rovněž funkční)
5. Vygenerujte projekt

Visual Studio:
6. Nastavte gmu_projekt jako StartUp Project
7. Spusťte projekt (Release, 64bit)

Poznámky
--------
Na Ubuntu 18.04, platformě ROCm 2.0 (RX 580) z neznámého důvodu nefunguje bitonic sort na větších polích (1024*128 a větší) s chybou: Memory access fault by GPU node-1 (Agent handle: 0x55df81ce7660) on address 0x501000000. Reason: Page not present or supervisor privilege. Překlad s #define LINUX_ROCM_2_0 bitonic sort přeskočí a je možné tak testovat větší velikosti řazeného pole pro ostatní algoritmy.
Spouštěné testy je možné změnit úpravou definice proměnné tests.
Nastavením proměnné show_stats na true je možné spustit výpis vlastností OpenCL zařízení (kód převzat z https://github.com/sivagnanamn/opencl-device-info).
